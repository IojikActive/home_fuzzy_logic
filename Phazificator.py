import logging

from abc import abstractclassmethod, ABC
from typing import Type, Dict, List
from dataclasses import dataclass

@dataclass
class AirState:
    x_target: float = 0     # x цели 
    x_jet: float = 0        # x истребителя
    x_required: float = 0   # x необходимые для поддержки линии визирования 


class BasePhazificator(ABC):

    
    def __init__(self, current_state: Type[]) -> None:
        pass